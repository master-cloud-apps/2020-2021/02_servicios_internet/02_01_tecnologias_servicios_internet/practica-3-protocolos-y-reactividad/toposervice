package org.eyo.toposervice;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CityDTO {

    private String id;
    private String landscape;
}
