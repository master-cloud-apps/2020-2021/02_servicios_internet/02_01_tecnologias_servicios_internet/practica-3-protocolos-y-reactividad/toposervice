package org.eyo.toposervice;

import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import javax.annotation.PostConstruct;

@Component
public class DBInitializer {

    private TopographicDetailsCrudRepository repository;

    public DBInitializer(TopographicDetailsCrudRepository repository) {
        this.repository = repository;
    }

    @PostConstruct
    public void initDatabase() {

        this.repository.deleteAll().block();

        Flux<TopographicDetails> users = Flux.just(
                new TopographicDetails(null, "Madrid", "flat"),
                new TopographicDetails(null, "Gijon", "mountain"),
                new TopographicDetails(null, "Barcelona", "mountain"),
                new TopographicDetails(null, "Sevilla", "mountain"),
                new TopographicDetails(null, "Valencia", "mountain"),
                new TopographicDetails(null, "Bilbao", "mountain"),
                new TopographicDetails(null, "Malaga", "mountain"),
                new TopographicDetails(null, "Combarro", "flat"),
                new TopographicDetails(null, "Caldas", "flat"),
                new TopographicDetails(null, "Vilagarcia", "flat"),
                new TopographicDetails(null, "Sanxenxo", "flat"),
                new TopographicDetails(null, "Cuntis", "flat"),
                new TopographicDetails(null, "Oviedo", "flat"),
                new TopographicDetails(null, "Alava", "flat"));

        users
                .flatMap(this.repository::save)
                .blockLast();
    }
}
