package org.eyo.toposervice;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
class RepositoryTest {

    @Autowired
    private TopographicDetailsCrudRepository repository;

    @Test
    void givenLandscape_whenFindAllByValue_thenFindTopographicDetails() {
        this.repository.save(new TopographicDetails(null, "Madrid", "Flat")).block();

        Flux<TopographicDetails> topoFlux = repository.findAllByLandscape("Flat");

        StepVerifier.create(topoFlux)
                .assertNext(topo -> {
                    assertEquals("Madrid", topo.getCity());
                    assertNotNull(topo.getId());
                })
                .expectComplete()
                .verify();
    }
}
